package com.example.logincompose.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.logincompose.R
import com.example.logincompose.ui.theme.primaryColor
import com.example.logincompose.ui.theme.whiteBackground

@Composable
fun LoginPage() {

    val emailValue = remember {
        mutableStateOf("")
    }

    val passwordValue = remember {
        mutableStateOf("")
    }

    val passwordVisibility = remember {
        mutableStateOf(false)
    }

    val showErrorEmail = remember {
        mutableStateOf(false)
    }

    val showErrorPassword = remember {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        Image(
            painter = painterResource(id = R.drawable.register_page),
            contentDescription = null
        )

        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .clip(shape = RoundedCornerShape(topStart = 30.dp, topEnd = 30.dp))
                .background(
                    whiteBackground
                )
                .padding(10.dp)
                .verticalScroll(rememberScrollState())
        ) {

            Text("Sign In", style = TextStyle(fontWeight = FontWeight.Bold, fontSize = 30.sp))
            Spacer(modifier = Modifier.padding(20.dp))
            Column(horizontalAlignment = Alignment.Start) {
                OutlinedTextField(
                    value = emailValue.value,
                    onValueChange = {
                        emailValue.value = it
                        showErrorEmail.value = emailValue.value.length < 4
                    },
                    label = { Text("Email Address", style = TextStyle(color = Color.Black)) },
                    placeholder = { Text("Email Address") },
                    singleLine = true,
                    modifier = Modifier.fillMaxWidth(0.8f)
                )
                if (showErrorEmail.value) Text("Email wrong !", style = TextStyle(color = Color.Red))
                Spacer(modifier = Modifier.padding(10.dp))
                OutlinedTextField(
                    value = passwordValue.value,
                    onValueChange = {
                        passwordValue.value = it
                        showErrorPassword.value = passwordValue.value.length < 6
                    },
                    trailingIcon = {
                        IconButton(onClick = {
                            passwordVisibility.value = !passwordVisibility.value
                        }) {
                            Icon(
                                painter = painterResource(id = R.drawable.password_eye),
                                contentDescription = null,
                                tint = if (passwordVisibility.value) primaryColor else Color.Gray
                            )
                        }
                    },
                    label = { Text("Password", style = TextStyle(color = Color.Black)) },
                    placeholder = { Text("Password") },
                    singleLine = true,
                    modifier = Modifier.fillMaxWidth(0.8f),
                    visualTransformation = if (passwordVisibility.value) VisualTransformation.None else PasswordVisualTransformation(),
                )
                if (showErrorPassword.value) Text("Password wrong !", style = TextStyle(color = Color.Red))
                Spacer(modifier = Modifier.padding(10.dp))
                Button(
                    onClick = {}, modifier = Modifier
                        .fillMaxWidth(0.8f)
                        .height(50.dp)
                ) {
                    Text(text = "Sign In")
                }
                Spacer(modifier = Modifier.padding(20.dp))
            }
        }
    }
}