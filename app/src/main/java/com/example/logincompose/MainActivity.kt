package com.example.logincompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.logincompose.ui.theme.LoginComposeTheme
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import com.example.logincompose.composables.LoginPage


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApp { LoginPage() }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MyApp { LoginPage() }
}

@Composable
fun MyApp(content: @Composable () -> Unit) {
    LoginComposeTheme {
        Surface(color = Color.White) {
            content()
        }
    }
}

@Composable
fun NewsStory() {
    MaterialTheme {
        val typography = MaterialTheme.typography
        val counterState = remember {
            mutableStateOf(0)
        }
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.header),
                contentDescription = null,
                modifier = Modifier
                    .height(180.dp)
                    .fillMaxWidth()
                    .clip(shape = RoundedCornerShape(10.dp)),
                contentScale = ContentScale.Crop

            )
            Spacer(Modifier.height(16.dp))
            Row {
                Surface(
                    color = Color.Black,
                    shape = RoundedCornerShape(6.dp),
                ) {
                    Text(
                        "Anh DT",
                        style = typography.caption,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis,
                        modifier = Modifier
                            .padding(8.dp)
                            .padding(horizontal = 20.dp)
                    )
                }
                Spacer(modifier = Modifier.width(20.dp))
                Counter(
                    typography = typography,
                    count = counterState.value,
                    updateCount = { newCount ->
                        counterState.value = newCount
                    })
            }
            Spacer(modifier = Modifier.height(10.dp))
            Divider(color = Color.Gray)
            Text("3 minutes ago", style = typography.body2)
        }
    }
}

@Composable
fun Counter(typography: Typography, count: Int, updateCount: (Int) -> Unit) {

    Button(onClick = { updateCount(count + 1) }) {
        Text(
            text = "I've been clicked $count times",
            maxLines = 1,
            style = typography.caption
        )
    }
}

@Composable
fun FlexibleLayout() {
    val names = List(1000) { "Compose jetpack" }
    Column(modifier = Modifier.fillMaxHeight()) {
        NewsStory()
        NameList(names = names, Modifier.weight(1f))
    }
}

@Composable
fun NameList(names: List<String>, modifier: Modifier = Modifier) {
    var isSelected by remember { mutableStateOf(false) }
    val backgroundColor by animateColorAsState(if (isSelected) Color.Red else Color.Transparent)
    LazyColumn(modifier = modifier) {
        itemsIndexed(names) { index, name ->
            Greeting(
                name = "$name $index",
                modifier = Modifier
                    .padding(24.dp)
                    .background(color = backgroundColor)
                    .clickable { isSelected = !isSelected })
            Divider(color = Color.Gray)
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {

    Text(
        text = "Hello $name",
        modifier = modifier
    )
}
